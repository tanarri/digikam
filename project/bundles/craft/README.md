Craft CI/CD Repository
----------------------

KDE uses a common framework named craft to process applications CI and CD workflow on different platforms.
The workflow supports notarization of application bundles for Windows and MacOS stores.

- Front page:       https://community.kde.org/Craft
- Configuration:    https://invent.kde.org/packaging/craft-blueprints-kde/-/tree/master/extragear/digikam
- Settings details: https://community.kde.org/Craft/Blueprints
